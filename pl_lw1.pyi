# Headers of symbols provided by asm library
from typing import overload, Tuple, NoReturn


def exit(code: int) -> NoReturn: ...


def string_length(s: bytes | bytearray, /) -> int: ...


def print_string(s: bytes | bytearray, /): ...


def print_char(ordinal: int, /) -> None: ...


def print_newline() -> None: ...


def print_uint(n: int, /) -> None: ...


def print_int(n: int, /) -> None: ...


def string_equals(a: bytes | bytearray, b: bytes | bytearray, /) -> bool: ...


def read_char() -> str: ...


@overload
def read_word(max_length: int, /) -> bytes | None: ...


@overload
def read_word(max_length: int, destination: bytearray, offset: int = ..., /) -> bytearray | None: ...


def parse_uint(string: bytes | bytearray) -> Tuple[int, int]: ...


def parse_int(string: bytes | bytearray) -> Tuple[int, int]: ...


@overload
def string_copy(source: bytes | bytearray) -> bytes: ...


@overload
def string_copy(source: bytes | bytearray, destination: bytearray, size: int = ..., /) -> bytearray | None: ...


@overload
def string_copy(source: bytes | bytearray, destination: bytearray, offset: int, size: int, /) -> bytearray | None: ...
