section .text



    ; Wrapper for eliminating name collision with exit from <stdlib.h>
    ; Arguments:
    ;   rdi (long long) - exit code
    ; Returns:
    ;   void
    global cwrapper_exit
cwrapper_exit:
    jmp exit

    ; Exits process with specified code
    ; Arguments:
    ;   rdi (long long) - exit code
    ; Returns:
    ;   void
    global exit
exit:
    ; Calling 'sys_exit'
    mov rax, 60
    syscall
    ; Sentinel returning
    ret



    ; Calcs length of null-terminated string
    ; Arguments:
    ;   rdi (char const *) - null-terminated string
    ; Returns:
    ;   rax (size_t) - length of string
    ; Locals:
    ;   rax - pointer to next char to fetch
    ;   rdi - start of string
    ;   dl - current char
    global string_length
string_length:
    ; Initializing local variables
    mov rax, rdi
    xor rdx, rdx
    ; Counter loop
string_length$L1:
    ; Check string if string is ended
    mov dl, BYTE [rax]
    test dl, dl
    jz string_length$L2                ; break
    ; Updating pointer and counter
    inc rax
    jmp string_length$L1               ; continue

string_length$L2:
    sub rax, rdi
    ret



    ; Prints null-terminated string to stdout (stream 1)
    ; Arguments:
    ;   rdi (char const *) - null-terminated string
    ; Returns:
    ;   void
    global print_string
print_string:
    ; Getting string length
    push rdi
    call string_length
    ; Calling 'sys_write'
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    pop rsi
    syscall
    ret



    ; Prints char to stdout (stream 1)
    ; Arguments:
    ;   rdi (char) - char to print
    ; Returns:
    ;   void
    global print_char
print_char:
    ; Allocating on stack string with length 1 and moving char to it
    sub rsp, 1
    mov BYTE [rsp], dil
    ; Calling 'sys_write'
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    ; Deallocating string
    add rsp, 1
    ret



    ; Just prints new line to stdout (stream 1)
    ; Arguments:
    ;   void
    ; Returns:
    ;   void
    global print_newline
print_newline:
    mov dil, 0xA
    call print_char
    ret



    ; Prints unsigned integer to stdout (stream 1)
    ; Arguments:
    ;   rdi (unsigned long long) - number to print
    ; Returns:
    ;   void
    ; Locals:
    ;   rax - trimmed number
    ;   rcx - constant '10' for div command
    ;   dl - current digit
    ;   r8 - pointer to end of local string
    global print_uint
print_uint:
    ; Check if number is zero and just print it
    test rdi, rdi
    jnz print_uint$L2
    mov dil, '0'
    call print_char
    ret

print_uint$L2:
    ; Initializing registers
    mov rax, rdi
    mov r8, rsp
    mov rcx, 10
    ; String generator loop
print_uint$L1:
    ; Generating digit
    xor rdx, rdx                       ; clearing remainder, otherwise will be fp error
    div rcx
    add rdx, '0'                       ; converting remainder to char
    ; Adding char to string on stack
    dec rsp
    mov BYTE [rsp], dl
    ; Checking if number ended
    test rax, rax
    jnz print_uint$L1                  ; continue
    ; Printing number
    sub r8, rsp
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, r8
    syscall
    ; Deallocating string
    add rsp, r8
    ret



    ; Prints signed integer to stdout (stream 1)
    ; Arguments:
    ;   rdi (long long) - number to print
    ; Returns:
    ;   void
    ; Locals:
    ;   rax - trimmed number
    ;   rcx - constant '10' for div command
    ;   dl - current digit
    ;   r8 - pointer to end of local string
    ;   r9 - flag if number is negative and '-' should be added
    global print_int
print_int:
    ; Determining half of number line
    cmp rdi, 0
    je print_int$L1
    jg print_int$L2
    ; Number is negative, changing sign
    neg rdi
    mov rax, rdi
    mov r9, 1
    jmp print_int$L3

print_int$L1:
    ; Printing zero
    mov dil, '0'
    call print_char
    ret

print_int$L2:
    ; Positive number branch
    mov rax, rdi
    xor r9, r9
print_int$L3:
    mov r8, rsp
    mov rcx, 10
    ; String generation loop
print_int$L4:
    ; Generating digit
    xor rdx, rdx                       ; clearing remainder, otherwise will be fp error
    div rcx
    add rdx, '0'                       ; converting remainder to char
    ; Adding char to string on stack
    sub rsp, 1
    mov BYTE [rsp], dl
    ; Checking if number is ended
    test rax, rax
    jnz print_int$L4
    ; Adding '-' if needed
    test r9, r9
    jz print_int$L5
    sub rsp, 1
    mov BYTE [rsp], '-'
print_int$L5:
    ; Printing generated string
    sub r8, rsp
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, r8
    syscall
    ; Deallocating string
    add rsp, r8
    ret



    ; Compares two null-terminated strings
    ; Arguments:
    ;   rdi (char const *) - first string
    ;   rsi (char const *) - second string
    ; Returns:
    ;   rax (int) - 1 if strings equals, otherwise 0
    ; Locals:
    ;   rdi - pointer to current char in first string
    ;   rsi - pointer to current char in second string
    ;   r8b - temporary char
    global string_equals
string_equals:
    xor r8, r8                          ; initializing register to prevent comparing issues
    ; String comparing loop
string_equals$L1:
    mov r8b, BYTE [rdi]                ; loading char because it will be used more then one time
    cmp r8b, BYTE [rsi]
    jne string_equals$L2               ; break
    ; Checking if string ended
    cmp r8, r8
    jz string_equals$L3                ; break
    ; Moving pointers
    add rdi, 1
    add rsi, 1
    jmp string_equals$L1               ; continue

string_equals$L3:
    mov rax, 1
    ret

string_equals$L2:
    xor rax, rax
    ret



    ; Reads one char from stdin (stream 0)
    ; Arguments:
    ;   void
    ; Returns
    ;   rax (char) - fetched char
    global read_char
read_char:
    ; Allocating buffer on stack
    sub rsp, 1
    ; Calling 'sys_read'
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    ; Checking if stream is ended - returning null char
    cmp rax, 1
    jne read_char$L1
    xor rax, rax
    mov al, BYTE [rsp]
    ; Deallocating buffer
    add rsp, 1
    ret

read_char$L1:
    ; Deallocating buffer
    add rsp, 1
    xor rax, rax
    ret



    ; Wrapper to reading word from C
    ;   rdi (char *) - buffer for saving word
    ;   rsi (size_t *) - read capacity of buffer by pointer and saves word length to it
    ; Returns:
    ;   rax (char *) - buffer if word fetched, otherwise NULL
    global cwrapper_read_word
cwrapper_read_word:
    push rsi
    mov rsi, QWORD [rsi]
    call read_word
    pop rsi
    mov QWORD [rsi], rdx
    ret

    ; Reads word from stdin (stream 0) skipping leading space chars
    ; Arguments:
    ;   rdi (char *) - buffer for saving word
    ;   rsi (size_t) - capacity of buffer
    ; Returns:
    ;   rax (char *) - buffer if word fetched, otherwise NULL
    ;   rdx (size_t) - length of fetched word
    ; Locals:
    ;   rax - current char
    ;   rcx - count of unset chars
    ;   rdx - read word size
    ;   rdi - pointer to current position in buffer
    global read_word
read_word:
    xor rax, rax         ; Clearing rax to prevent issues with comparing
    ; Check if buffer has zero size - can't find words
    test rsi, rsi
    jnz read_word$L1
    xor rdx, rdx
    ret

read_word$L1:
    push rsi           ; Saving buffer size
    ; Skipping leading space chars
read_word$L2:
    push rdi           ; Saving pointer to buffer
    ; Calling 'sys_read'
    mov rsi, rdi       ; Reading char to first pos in buffer
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    syscall
    ; Check if stream ended - no words left
    cmp rax, 1
    je read_word$L3
    add rsp, 16        ; Removing buffer pointer and size from stack
    xor rax, rax       ; Return NULL
    xor rdx, rdx
    ret

read_word$L3:
    ; Comparing with space chars codes
    pop rdi
    mov al, BYTE [rdi]
    cmp al, `\n`      ; Compare with \n
    je read_word$L2
    cmp al, `\t`      ; Compare with \t
    je read_word$L2
    cmp al, ' '       ; Compare with space
    je read_word$L2
    jmp read_word$L4

read_word$L4:
    ; Starting fetching word
    pop rcx
    push rdi           ; Saving pointer to start
    jmp read_word$L9

read_word$L5:
    ; No space in buffer, word not fetched
    test rcx, rcx
    jnz read_word$L6
    add rsp, 8
    xor rax, rax
    xor rdx, rdx
    ret

read_word$L6:
    push rcx           ; Saving counter
    push rdi           ; Savin current pos pointer
    ; Calling 'sys_read'
    mov rsi, rdi       ; Reading char to current pos in buffer
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    syscall
    ; Check if stream ended - word ended
    cmp rax, 1
    je read_word$L7
    pop rdx
    mov BYTE [rdx], 0
    add rsp, 8
    pop rax
    sub rdx, rax       ; Chars have size 1, so word length is just difference between end and start pointers
    ret

read_word$L7:
    pop rdi
    ; Comparing with space chars - end of word
    mov al, BYTE [rdi]
    cmp al, `\n`      ; Compare with \n
    je read_word$L8
    cmp al, `\t`      ; Compare with \t
    je read_word$L8
    cmp al, ' '       ; Compare with space
    je read_word$L8
    pop rcx
read_word$L9:
    mov BYTE [rdi], al
    ; Moving pointers
    inc rdi
    dec rcx
    jmp read_word$L5

read_word$L8:
    ; Setting trailing null-char
    ; Buffer have space because otherwise this branch unreachable
    mov BYTE [rdi], 0
    add rsp, 8                         ; Discarding counter
    mov rdx, rdi
    pop rax
    sub rdx, rax
    ret



    ; Wrapper to calling function from C code
    ; Arguments:
    ;   rdi (char const *) - string with number
    ;   rsi (size_t *) - size of parsed number
    ; Returns:
    ;   rax (unsigned long long) - parsed number
    global cwrapper_parse_uint
cwrapper_parse_uint:
    push rsi
    call parse_uint
    pop rsi
    mov QWORD [rsi], rdx
    ret

    ; Wrapper to calling function from C code
    ; Arguments:
    ;   rdi (char const *) - string with number
    ;   rsi (size_t *) - size of parsed number
    ; Returns:
    ;   rax (long long) - parsed number
    global cwrapper_parse_int
cwrapper_parse_int:
    push rsi
    call parse_int
    pop rsi
    mov QWORD [rsi], rdx
    ret

    ; Parses signed integer from string
    ; Entry point to number parser
    ; Arguments:
    ;   rdi (char const *) - string with number
    ; Returns:
    ;   rax (long long) - parsed number
    ;   rdx (size_t) - size of parsed number in chars
    ; Locals:
    ;   rax - accumulator
    ;   rcx - count of chars consumed
    ;   rdi - pointer to current char
    ;   r8 - constant '10' for 'mul' command
    ;   r9b - current char
    ;   r10 - pointer to start of string, need for check if just string contains only '-'
    global parse_int
parse_int:
    ; Initializing registers
    xor rcx, rcx
    xor rax, rax
    mov r8, 10
    xor r10, r10
    xor r9, r9
    ; Check if first char is '-'
    mov r9b, BYTE [rdi]
    cmp r9b, '-'
    jne parse_int$L1
    mov r10, rdi
    jmp parse_int$L3

    ; Parses unsigned integer from string
    ; Entry point to number parser
    ; Arguments:
    ;   rdi (char const *) - string with number
    ; Returns:
    ;   rax (unsigned long long) - parsed number
    ;   rdx (size_t) - size of parsed number in chars
    ; Locals:
    ;   rax - accumulator
    ;   rcx - count of chars consumed
    ;   rdi - pointer to current char
    ;   r8 - constant '10' for 'mul' command
    ;   r9b - current char
    ;   r10 - pointer to start of string, need for check if just string contains only '-'
    global parse_uint
parse_uint:
    ; Initializing registers
    xor rax, rax
    xor rcx, rcx
    mov r8, 10
    xor r10, r10
    ; Parser loop
parse_uint$L1:
    ; Fetching char
    xor r9, r9
    mov r9b, BYTE [rdi]
parse_int$L1:
    ; Parsing char
    cmp r9b, '0'
    jl parse_uint$L2
    cmp r9b, '9'
    jg parse_uint$L2
    sub r9b, '0'
    ; Updating accumulator
    mul r8
    add rax, r9
parse_int$L3:
    ; Moving pointers
    add rdi, 1
    add rcx, 1
    jmp parse_uint$L1

parse_uint$L2:
    cmp r10, 0                         ; if number is negative (r10 != NULL)
    je parse_int$L2
    ; Checking that pointer not on the next position after start
    add r10, 1
    cmp rdi, r10
    je parse_int$L4
    ; Inverting sign of number
    mov rdx, rax
    xor rax, rax
    sub rax, rdx
parse_int$L2:
    mov rdx, rcx
    ret

parse_int$L4:
    xor rdx, rdx
    ret



    ; Copies string to buffer
    ; Arguments:
    ;   rdi (char const *) - source string
    ;   rsi (char *) - destination buffer
    ;   rdx (size_t) - buffer capacity
    ; Returns:
    ;   rax (size_t) - length of string, if fits to buffer, otherwise 0
    ; Locals:
    ;   al - current char to copy
    ;   rcx - count of unset chars in buffer
    ;   rdx - initial buffer capacity, for calc string length by rcx
    ;   rdi - pointer to current char in string
    ;   rsi - pointer to current char in buffer
    global string_copy
string_copy:
    ; Initializing registers
    mov rcx, rdx
    xor rax, rax
string_copy$L1:
    mov al, BYTE [rdi]                 ; saving char to register because it will be used more than one time
    ; Checking that string not ended yet
    test al, al
    jz string_copy$L2
    ; Checking that buffer have space to store char
    test rcx, rcx
    jz string_copy$L3
    ; Storing char
    mov BYTE [rsi], al
    ; Moving pointers
    inc rdi
    inc rsi
    inc rcx
    jmp string_copy$L1                 ; continue

string_copy$L2:
    ; Check if buffer have space to store trailing null-char
    test rcx, rcx
    jz string_copy$L3
    ; Saving null-char
    mov BYTE [rsi], 0
    mov rax, rdx
    sub rax, rcx
    ret

string_copy$L3:
    xor rax, rax
    ret