#include <Python.h>

extern void cwrapper_exit(long long code);

static PyObject *pywrapper_exit(PyObject *Py_UNUSED(module), PyObject *code_o)
{
    long long code;
    if (PyLong_Check(code_o))
    {
        code = PyLong_AsLongLong(code_o);
    }
    else
    {
        PyErr_Format(PyExc_TypeError, "Exit code must be 'int'");
        return NULL;
    }

    cwrapper_exit(code);

    Py_UNREACHABLE();
}

extern size_t string_length(char *s);

static PyObject *pywrapper_string_length(PyObject *Py_UNUSED(module), PyObject *s)
{
    char *data;
    if (PyBytes_Check(s))
    {
        data = PyBytes_AS_STRING(s);
    }
    else if (PyByteArray_Check(s))
    {
        data = PyByteArray_AS_STRING(s);
    }
    else
    {
        PyErr_Format(PyExc_TypeError, "String must be 'bytes' of 'bytearray'");
        return NULL;
    }

    return PyLong_FromUnsignedLongLong(string_length(data));
}

extern void print_string(char *s);

static PyObject *pywrapper_print_string(PyObject *Py_UNUSED(module), PyObject *s)
{
    char *data;

    if (PyBytes_Check(s))
    {
        data = PyBytes_AS_STRING(s);
    }
    else if (PyByteArray_Check(s))
    {
        data = PyByteArray_AS_STRING(s);
    }
    else
    {
        PyErr_Format(PyExc_TypeError, "String must be 'bytes' of 'bytearray'");
        return NULL;
    }

    print_string(data);

    Py_RETURN_NONE;
}

extern void print_char(char ordinal);

static PyObject *pywrapper_print_char(PyObject *Py_UNUSED(module), PyObject *ordinal_o)
{
    if (!PyLong_Check(ordinal_o))
    {
        PyErr_Format(PyExc_TypeError, "Ordinal must be 'int'");
        return NULL;
    }
    long ordinal = PyLong_AsLong(ordinal_o);
    if (ordinal == -1 && PyErr_Occurred())
    {
        return NULL;
    }

    if (ordinal < 0 || ordinal > 127)
    {
        PyErr_Format(PyExc_ValueError, "Function can print only ASCII characters, so ordinal must be in range [0, 127]");
        return NULL;
    }

    print_char((char) ordinal);

    Py_RETURN_NONE;
}

extern void print_newline(void);

static PyObject *pywrapper_print_newline(PyObject *Py_UNUSED(module))
{
    print_newline();

    Py_RETURN_NONE;
}


extern void print_uint(unsigned long long n);

static PyObject *pywrapper_print_uint(PyObject *Py_UNUSED(module), PyObject *o)
{
    if (!PyLong_Check(o))
    {
        PyErr_Format(PyExc_TypeError, "Number must be 'int'");
        return NULL;
    }

    PyObject *zero = PyLong_FromLong(0);
    if (zero == NULL)
    {
        return NULL;
    }

    unsigned long long n = PyLong_AsUnsignedLongLong(o);
    if (n == (unsigned long long) -1 && PyErr_Occurred())
    {
        return NULL;
    }

    print_uint(n);

    Py_RETURN_NONE;
}

extern void print_int(long long n);

static PyObject *pywrapper_print_int(PyObject *Py_UNUSED(module), PyObject *o)
{
    if (!PyLong_Check(o))
    {
        PyErr_Format(PyExc_TypeError, "Number must be 'int'");
        return NULL;
    }

    long long n = PyLong_AsLongLong(o);
    if (n == -1 && PyErr_Occurred())
    {
        return NULL;
    }

    print_int(n);

    Py_RETURN_NONE;
}

extern int string_equals(char *s1, char *s2);

static PyObject *pywrapper_string_equals(PyObject *Py_UNUSED(module), PyObject *args)
{
    PyObject *s1o;
    PyObject *s2o;
    if (!PyArg_ParseTuple(args, "OO", &s1o, &s2o))
    {
        return NULL;
    }

    char *s1;
    if (PyBytes_Check(s1o))
    {
        s1 = PyBytes_AS_STRING(s1o);
    }
    else if (PyByteArray_Check(s1o))
    {
        s1 = PyByteArray_AS_STRING(s1o);
    }
    else
    {
        PyErr_Format(PyExc_TypeError, "First string must be 'bytes' of 'bytearray'");
        return NULL;
    }

    char *s2;
    if (PyBytes_Check(s2o))
    {
        s2 = PyBytes_AS_STRING(s2o);
    }
    else if (PyByteArray_Check(s2o))
    {
        s2 = PyByteArray_AS_STRING(s2o);
    }
    else
    {
        PyErr_Format(PyExc_TypeError, "Second string must be 'bytes' of 'bytearray'");
        return NULL;
    }

    if (string_equals(s1, s2))
        Py_RETURN_TRUE;
    else
        Py_RETURN_FALSE;
}

extern char read_char(void);

PyObject *pywrapper_read_char(PyObject *Py_UNUSED(module))
{
    char c = read_char();
    return PyUnicode_FromStringAndSize(&c, 1);
}

extern char *cwrapper_read_word(char *buffer, size_t *buffer_size);

// extern char *read_word(char *buffer, size_t buffer_size);

extern size_t string_copy(char *src, char *dst, size_t buffer_size);

PyObject *pywrapper_read_word(PyObject *Py_UNUSED(module), PyObject *args)
{
    size_t buffer_size;
    PyObject *buffer_o = NULL;
    Py_ssize_t offset = -1;
    if (!PyArg_ParseTuple(args, "K|O!n", &buffer_size, &PyByteArray_Type, &buffer_o, &offset))
    {
        return NULL;
    }

    char *buffer = PyMem_Malloc(buffer_size);
    if (buffer == NULL)
    {
        return PyErr_NoMemory();
    }

    if (cwrapper_read_word(buffer, &buffer_size) == NULL)
    {
        PyMem_Free(buffer);
        Py_RETURN_NONE;
    }

    if (buffer_o == NULL)
    {
        buffer_o = PyBytes_FromStringAndSize(buffer, (Py_ssize_t) buffer_size);
        PyMem_Free(buffer);
        return buffer_o;
    }
    else
    {
        Py_ssize_t start = offset >= 0 ? offset : PyByteArray_GET_SIZE(buffer_o);

        if (start + buffer_size > PyByteArray_GET_SIZE(buffer_o))
        {
            if (PyByteArray_Resize(buffer_o, start + (Py_ssize_t) buffer_size))
            {
                PyMem_Free(buffer);
                return NULL;
            }
        }

        if (string_copy(buffer, PyByteArray_AS_STRING(buffer_o) + start, buffer_size) != buffer_size)
        {
            PyMem_Free(buffer);
            Py_UNREACHABLE();
        }

        PyMem_Free(buffer);
        Py_INCREF(buffer_o);
        return buffer_o;
    }
}

PyObject *pywrapper_string_copy(PyObject *Py_UNUSED(module), PyObject *args)
{
    if (args != NULL && PyTuple_GET_SIZE(args) == 1)
    {
        PyObject *source_o = PyTuple_GET_ITEM(args, 0);
        char *source;
        size_t buffer_size;
        char *buffer;

        if (PyBytes_Check(source_o))
        {
            source = PyBytes_AS_STRING(source_o);
            buffer_size = PyBytes_GET_SIZE(source_o);
        }
        else if (PyByteArray_Check(source_o))
        {
            source = PyByteArray_AS_STRING(source_o);
            buffer_size = PyByteArray_GET_SIZE(source_o);
        }
        else
        {
            PyErr_Format(PyExc_TypeError, "String must be 'bytes' of 'bytearray'");
            return NULL;
        }

        buffer = PyMem_Malloc(buffer_size);
        if (buffer == NULL)
            return PyErr_NoMemory();

        if (string_copy(source, buffer, buffer_size) != buffer_size)
        {
            PyMem_Free(buffer);
            Py_UNREACHABLE();
        }

        source_o = PyBytes_FromStringAndSize(buffer, (Py_ssize_t) buffer_size);
        PyMem_Free(buffer);
        return source_o;
    }


    PyObject *source_o;
    PyObject *buffer_o;
    Py_ssize_t offset = -1;
    Py_ssize_t size = -1;
    if (!PyArg_ParseTuple(args, "OO!|nn", &source_o, &PyByteArray_Type, &buffer_o, &offset, &size))
    {
        return NULL;
    }

    if (size < 0)
    {
        size = offset;
        offset = -1;
    }

    char *source;
    if (PyBytes_Check(source_o))
    {
        source = PyBytes_AS_STRING(source_o);
        if (size < 0)
            size = PyBytes_GET_SIZE(source_o);
    }
    else if (PyByteArray_Check(source_o))
    {
        source = PyByteArray_AS_STRING(source_o);
        if (size < 0)
            size = PyByteArray_GET_SIZE(source_o);
    }
    else
    {
        PyErr_Format(PyExc_TypeError, "String must be 'bytes' of 'bytearray'");
        return NULL;
    }

    if (size == 0)
    {
        Py_INCREF(buffer_o);
        return buffer_o;
    }


    if (offset < 0)
    {
        offset = PyByteArray_GET_SIZE(buffer_o);
        if (PyByteArray_Resize(buffer_o, offset + size))
            return NULL;
    }
    else
    {
        if (offset + size > PyByteArray_GET_SIZE(buffer_o))
        {
            if (PyByteArray_Resize(buffer_o, offset + size))
                return NULL;
        }
    }


    char *buffer = PyByteArray_AsString(buffer_o);
    if (string_copy(source, buffer + offset, size) == 0)
    {
        Py_RETURN_NONE;
    }
    Py_INCREF(buffer_o);
    return buffer_o;
}

extern unsigned long long cwrapper_parse_uint(char *string, size_t *number_length);

PyObject *pywrapper_parse_uint(PyObject *Py_UNUSED(module), PyObject *string_o)
{
    char *string;
    if (PyBytes_Check(string_o))
    {
        string = PyBytes_AS_STRING(string_o);
    }
    else if (PyByteArray_Check(string_o))
    {
        string = PyByteArray_AS_STRING(string_o);
    }
    else
    {
        PyErr_Format(PyExc_TypeError, "String must be 'bytes' of 'bytearray'");
        return NULL;
    }

    size_t number_size;
    unsigned long long number = cwrapper_parse_uint(string, &number_size);
    return Py_BuildValue("Kn", number, number_size);
}

extern long long cwrapper_parse_int(char *string, size_t *number_length);

PyObject *pywrapper_parse_int(PyObject *Py_UNUSED(module), PyObject *string_o)
{
    char *string;
    if (PyBytes_Check(string_o))
    {
        string = PyBytes_AS_STRING(string_o);
    }
    else if (PyByteArray_Check(string_o))
    {
        string = PyByteArray_AS_STRING(string_o);
    }
    else
    {
        PyErr_Format(PyExc_TypeError, "String must be 'bytes' of 'bytearray'");
        return NULL;
    }

    size_t number_size;
    long long number = cwrapper_parse_int(string, &number_size);
    return Py_BuildValue("Ln", number, number_size);
}

static PyMethodDef py_methods[] = {
        {"exit",          (PyCFunction) pywrapper_exit,          METH_O},
        {"string_length", (PyCFunction) pywrapper_string_length, METH_O},
        {"print_string",  (PyCFunction) pywrapper_print_string,  METH_O},
        {"print_char",    (PyCFunction) pywrapper_print_char,    METH_O},
        {"print_newline", (PyCFunction) pywrapper_print_newline, METH_NOARGS},
        {"print_uint",    (PyCFunction) pywrapper_print_uint,    METH_O},
        {"print_int",     (PyCFunction) pywrapper_print_int,     METH_O},
        {"string_equals", (PyCFunction) pywrapper_string_equals, METH_VARARGS},
        {"read_char",     (PyCFunction) pywrapper_read_char,     METH_NOARGS},
        {"read_word",     (PyCFunction) pywrapper_read_word,     METH_VARARGS},
        {"parse_uint",    (PyCFunction) pywrapper_parse_uint,    METH_O},
        {"parse_int",     (PyCFunction) pywrapper_parse_int,     METH_O},
        {"string_copy",   (PyCFunction) pywrapper_string_copy,   METH_VARARGS},
        {NULL}
};

static PyModuleDef py_module = {
        .m_name = "pl_lw1",
        .m_methods = py_methods
};

PyMODINIT_FUNC PyInit_pl_lw1(void)
{
    return PyModule_Create(&py_module);
}