nasm                                                                                              \
  -felf64                                                                                         \
  -o ./build/functions.o                                                                          \
  ./src/functions.asm

gcc                                                                                               \
  -nostdlib -shared -fPIC                                                                         \
  ./src/py_wrapper.c                                                                              \
  ./build/functions.o                                                                             \
  -I /usr/include/python3.8                                                                       \
  -l python3.8                                                                                    \
  -o pl_lw1.so